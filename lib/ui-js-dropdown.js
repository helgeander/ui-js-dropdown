(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
      define([], factory);
  } else if (typeof exports === "object") {
      module.exports = factory();
  } else {
    // Browser globals (root is window)
    root.UiJsDropdown = factory();
  }
}(this, function () {

  function createEl(tagName, parentEl, classes, construct) {
      var el = document.createElement(tagName);
      if (classes && typeof classes == 'string')
          el.className = classes;
      if (typeof construct == 'function')
          construct(el);
      parentEl.appendChild(el);
      return el;
  }

  function createOptionElement(nodeData, parentEl, index) {
      var that = this;
      return createEl.call(this, "div", parentEl, 'item', function(el) {
          var nodeContent = that.nodeRenderFn(nodeData, el);
          if (typeof nodeContent == 'object' && nodeContent.tagName)
              el.appendChild(nodeContent);
          else
              el.appendChild(document.createTextNode(nodeContent));
          el.setAttribute('id', 'ui-js-option-'+index);
          el.setAttribute('data-index', index);
      });
  }

  function getFilteredNodes(data, filter, limit) {
      if (typeof filter == 'function') {
          return data.filter(filter).slice(0, limit);
      } else
          return data.slice(0, limit);
  }

  function getOptionEl(el, recurse) {
    if (el.hasAttribute('data-index') && el.classList.contains('item'))
        return el;
    if (el.parentElement)
        return recurse(el.parentElement);
    return undefined;
  }

    function getScrollParent(node) {
        const isElement = node instanceof HTMLElement;
        const overflowY = isElement && window.getComputedStyle(node).overflowY;
        const isScrollable = overflowY !== 'visible' && overflowY !== 'hidden';

        if (!node) {
            return null;
        } else if (isScrollable && node.scrollHeight >= node.clientHeight) {
            return node;
        }

        return getScrollParent(node.parentNode) || window;
    }

    function moveToTop(el, inputHeight) {
        var container = getScrollParent(el.parentElement);
        var containerHeight = container ? container.clientHeight : window.innerHeight;
        var rect = el.getBoundingClientRect(),
            belowBottom = rect.bottom > containerHeight,
            fromTop = rect.top + inputHeight,
            fromBottom = containerHeight - rect.top;
        return belowBottom && fromTop > fromBottom;
    }

    function isVisibleInContainer(el) {
        var offsetEl = el.offsetParent;
        return offsetEl ?
            (offsetEl.clientHeight + offsetEl.scrollTop > (el.offsetTop + el.clientHeight) &&
            (offsetEl.scrollTop < (el.offsetTop + el.clientHeight))) : true;
    }

  /**
     * Create a ui dropdown control
     * @param {Array} options The dropdown options data objects
     * @param {Element} element The dropdown element to transform
     * @param {Object} config Dropdown configuration options
     */
  function UiJsDropdown(options, element, config) {
    var that = this;
    this.options = options;
    this.element = element;
    this.config = config;
    this.selected = null;
    this.selectedIndex = -1;
    this.nodeMap = {};
    this.onSelect = function() {};

    if (!this.element.classList.contains('ui-js-dropdown'))
        this.element.classList.add('ui-js-dropdown');

    var inputs = this.element.getElementsByTagName('input');
    this.inputEl = inputs && inputs.length > 0 ? inputs[0] : createEl.call(this, 'input', this.element, 'ui-js-dropdown-input', function(el) {
      el.setAttribute('type', 'text');
      el.addEventListener('focus', function(ev) {
        el.select();
      });
    });

    this.computedStyle = window.getComputedStyle(element);
    var topPadding = parseFloat(this.computedStyle.paddingTop),
        topMargin = parseFloat(this.computedStyle.marginTop),
        borderWidth = parseFloat(this.computedStyle.borderWidth);
    this.computedHeight = Math.round(parseFloat(this.computedStyle.height));
    this.computedTop = Math.round(parseFloat(this.computedStyle.top));
    this.fromBottom = Math.round(this.computedHeight + borderWidth + topMargin + topPadding + 2);

    var btns = this.element.getElementsByTagName('i');
    this.btnEl = btns && btns.length > 0 ? btns[0] : createEl.call(this, 'i', this.element, 'ui-js-dropdown-button', function(el) {
      el.style.top = Math.round(that.computedHeight / 2);
      that.element.addEventListener('click', function(ev) {
        if (that.isOptionsVisible())
            that.hideOptions();
        else {
            that.showOptions();
        }
      });
    });

    var opts = this.element.getElementsByClassName('ui-js-dropdown-options');
    this.optsEl = opts && opts.length > 0 ? opts[0] : createEl.call(this, 'div', this.element, 'ui-js-dropdown-options');

    this.optsEl.addEventListener('mousedown', function(ev) {
        that.lastEvent = 'mousedown';
        var targetEl = getOptionEl(ev.target, getOptionEl);
        if (targetEl) {
            var index = targetEl.getAttribute('data-index');
            that.setSelectedNode(parseInt(index));
            that.commitOrOpen();
        }
        ev.stopPropagation();
        ev.stopImmediatePropagation();
        ev.preventDefault();
    });

    this.nodeRenderFn = this.config.nodeRenderFn ? this.config.nodeRenderFn : function(data, el) { return data.text; };
    this.inputRenderFn = this.config.inputRenderFn ? this.config.inputRenderFn : function(data) { return data.text; };
    this.filterFn = this.config.filterFn ? this.config.filterFn : function(data, value) { return data.text.substr(0, value.length).toLowerCase() == value.toLowerCase(); };
    this.asList = this.config.list ? this.config.list : false;
    this.maxOptions = this.config.maxOptions ? this.config.maxOptions : -1;

    if (!this.inputEl)
      throw "Input element not found";

    this.keyMap = {
      nextNode: ['ArrowDown', 'Down'],
      prevNode: ['ArrowUp', 'Up'],
      firstNode: ['Home'],
      lastNode: ['End'],
      select: ['Enter'],
      cancel: ['Esc', 'Escape']
    };

      this.onSelect = config && typeof config.onSelect == 'function' ? config.onSelect : this.onSelect;
      this.selectedData = config && config.selectedData ? config.selectedData : undefined;

    Object.defineProperty(this, 'idCounter', {
      value: 0,
      writable: true,
      configurable: false,
      enumerable: false
    });

    this.element.addEventListener('focusout', function(ev) {
        if (that.isOptionsVisible() && ev.currentTarget == that.element) {
            that.commit();
        }
    });

    this.inputEl.addEventListener('input', function(ev) {
        if (that.propagateKeys.indexOf(ev.key) > -1)
            return;
        if (that.inputEl.value != '' && that.config.clientFiltering !== false) {
            that.filter(that.inputEl.value, 20);
            that.showOptions();
            that.firstNode();
        } else {
            that.render();
        }
    });

    this.inputEl.addEventListener('keydown', function(ev) {
        that.lastEvent = 'keydown';
        if (!that.propagateKeys) {
            that.propagateKeys = Object.keys(that.keyMap)
                .map(function(key) {
                    return that.keyMap[key];
                })
                .reduce(function(acc, cur) {
                    return acc.concat(cur);
                }, []);
        }

        if (that.propagateKeys.indexOf(ev.key) > -1) {
            ev.stopPropagation();
            ev.preventDefault();
        }

        if (that.keyMap.nextNode.indexOf(ev.key) > -1)
            that.nextNode();
        else if (that.keyMap.prevNode.indexOf(ev.key) > -1)
            that.prevNode();
        else if (that.keyMap.firstNode.indexOf(ev.key) > -1)
            that.firstNode();
        else if (that.keyMap.lastNode.indexOf(ev.key) > -1)
            that.lastNode();
        else if (that.keyMap.select.indexOf(ev.key) > -1)
            that.commitOrOpen();
        else if (that.keyMap.cancel.indexOf(ev.key) > -1)
            that.cancel();
    });
  }

  /**
   * Render the tree from the loaded data
   * @param {function} filter Filter predicate
   * @param {integer} limit Limit filter result by max count returned
   */
  UiJsDropdown.prototype.render = function(filter, limit) {
      this.clear();
      this.idCounter = 0;
      var el = this.containerEl;
      //Work with document fragment
      var frag = document.createDocumentFragment();

      //Filter nodes and return list
      this.visibleOptions = getFilteredNodes(this.options, filter, limit || this.maxOptions || 200);

      if (Array.isArray(this.visibleOptions))
        this.visibleOptions.forEach(function(dataItem, index) {
            createOptionElement.call(this, dataItem, frag, index);
          }, this);

      //Add all node elements (as document fragment) to the DOM
      this.optsEl.appendChild(frag);

      this.el = el;
  };

  /**
   * Clear list, remove all nodes and data
   */
  UiJsDropdown.prototype.clear = function() {
    var el = this.optsEl;
    this.selectedIndex = -1;
    delete this.selectedData;
    this.nodeMap = {};
    while (el.firstChild) {
        el.removeChild(el.firstChild);
    }
  }

  UiJsDropdown.prototype.isOptionsVisible = function() {
    return this.optsEl.classList.contains('show');
  };

  UiJsDropdown.prototype.showOptions = function() {
    if (!this.isOptionsVisible()) {
        this.optsEl.classList.add('show');
        this.optsEl.style.bottom = '';
        if (moveToTop(this.optsEl, this.computedHeight)) {
            this.optsEl.style.bottom = this.fromBottom + 'px';
        }
    }
  };

  UiJsDropdown.prototype.hideOptions = function() {
    if (this.isOptionsVisible())
        this.optsEl.classList.remove('show');
  };

  /**
   * Set selected node by predicate
   */
  UiJsDropdown.prototype.setSelectedNodeBy = function (predicate, clearOnMisssing) {
      var found = this.options.find(predicate);
      if (found) {
          this.selectedData = found;
          this.commit();
      } else if (clearOnMisssing) {
          this.selectedData = undefined;
          this.commit();
      }
  };

  /**
   * Set selected node
   */
  UiJsDropdown.prototype.setSelectedNode = function(index, up) {
    var lastSelected = this.selectedIndex;
    this.selectedIndex = index;
    if (lastSelected > -1 && lastSelected < this.visibleOptions.length)
        this.optsEl.childNodes[lastSelected].classList.remove('selected');

    if (index == -1) return;
    var el = this.optsEl.childNodes[index];
    el.classList.add('selected');
    if (!isVisibleInContainer(el)) {
        el.scrollIntoView(up);
    }
    this.selectedData = this.getData();
    return this.selectedData;
  };

  /**
   * Get selected data
   */
    UiJsDropdown.prototype.getData = function () {
        return this.selectedIndex > -1 ? this.visibleOptions[this.selectedIndex] : this.selectedData;
  };

  /**
   * Get selected node
   */
    UiJsDropdown.prototype.getSelectedNode = function () {
        return this.selectedIndex > -1 ? this.optsEl.childNodes[this.selectedIndex] : undefined;
  };

  /**
   * Select next node
   */
  UiJsDropdown.prototype.nextNode = function() {
    if (this.selectedIndex < this.optsEl.childNodes.length-1) {
        this.setSelectedNode(this.selectedIndex + 1, false);
        this.showOptions();
    }
  };

  /**
   * Select previous node
   */
  UiJsDropdown.prototype.prevNode = function() {
    if (this.selectedIndex >= 0)
        this.setSelectedNode(this.selectedIndex - 1, true);
  };

  /**
   * Select first node
   */
  UiJsDropdown.prototype.firstNode = function() {
    if (this.optsEl.childNodes.length > 0)
        this.setSelectedNode(0, true);
  };

  /**
   * Select last node
   */
  UiJsDropdown.prototype.lastNode = function() {
    if (this.optsEl.childNodes.length > 0)
        this.setSelectedNode(this.optsEl.childNodes.length - 1, false);
  };

  /**
   * Select node
   */
  UiJsDropdown.prototype.commit = function() {
    var data = this.getData();
    this.onSelect(data);
    this.inputEl.value = data ? this.inputRenderFn(data) : '';
    //this.inputEl.focus();
    this.hideOptions();
  };

  /**
   * Select node
   */
  UiJsDropdown.prototype.commitOrOpen = function() {
    if (this.selectedIndex >= 0) {
        this.commit();
    } else if (!this.isOptionsVisible()) {
        this.showOptions();
    }
  };

  /**
   * Cancel selection
   */
  UiJsDropdown.prototype.cancel = function() {
    this.selectedIndex = -1;
    this.hideOptions();
  };

  /**
   * Filter nodes, display flat list of nodes
   * @param {Function} filter Predicate to filter nodes
   * @param {Integer} limit Limit the displayed nodes
   */
  UiJsDropdown.prototype.filter = function(searchVal, limit) {
      var that = this;
      this.render(function(data) {
        return that.filterFn(data, searchVal);
      }, limit);
  };

  /**
   * Load new data into the dropdown options, replacing the existing options
   * @param {Object|Array} newData The new data to load
   */
  UiJsDropdown.prototype.load = function(newData) {
    this.idCounter = 0;
    this.options = newData;
    this.render();
  };

  return UiJsDropdown;
}));