
function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => { func.apply(this, args); }, timeout);
  };
}

/**
 * Default options provider
 */
class UiJsDefaultProvider {
  constructor(options) {
    this.optionsData = options;
  }

  async fetchOptions(search, size) {
    return this.optionsData
      .filter?.(data => data.text.toLowerCase().startsWith(search?.toLowerCase()) || data.value.toString().startsWith(search) || !search)
      .slice?.(0, size) || [];
  }
}

class UiJsDropDownOption extends HTMLElement {
  constructor(option, renderer) {
    super();
    this.renderer = renderer;
    if (option instanceof HTMLOptionElement) {
      this.data = { text: option.innerText, value: option.value || option.innerText };
      this.text = this.data.text;
      this.value = this.data.value;
    }
    else if (option) {
      this.data = option;
      this.text = option.text;
      this.value = option.value || option.text;
    }
  }

  connectedCallback() {
    this.tabIndex = -1;
    this.innerText = this.renderer(this.data);
  }
}

class UiJsDropDownOptions extends HTMLElement {
  constructor(options, optionRenderer) {
    super();
    this.loadOptions(options, optionRenderer, true);
  }

  connectedCallback() {
    this.hide();
    this.render();
  }

  render() {
    this.innerHTML = '';
    for (let opt of this.options)
      this.append(opt);
  }

  loadOptions(options, renderer, silent) {
    this.options = Array.from(options).map(o => new UiJsDropDownOption(o, renderer));

    if (!silent)
      this.render();
  }

  hide() {
    this.classList.add('hide');
  }

  show() {
    this.classList.remove('hide');
  }
}

customElements.define('ui-js-dropdown-option', UiJsDropDownOption);
customElements.define('ui-js-dropdown-options', UiJsDropDownOptions);
customElements.define('ui-js-dropdown', class extends HTMLElement {

    constructor() {
      super();

      this.search = debounce(() => this._search(this.input.value));
    }

    static get observedAttributes() {
      return ['open'];
    }

    connectedCallback() {
      this.setConfig();
      this.parseRenderers();
      this.parseOptions(this.children);

      const defaultOptions = this.getDefaultOptions();
      this.optionsEl = new UiJsDropDownOptions(defaultOptions, this.optionRenderFn);

      this.input = document.createElement('input');
      this.input.setAttribute('type','text');
      this.input.classList.add('ui-js-dropdown-input');

      this.spinner = document.createElement('div');
      this.spinner.classList.add('ui-js-dropdown-spinner');

      this.spinnerContent = document.createElement('div');
      this.spinnerContent.classList.add('dot-pulse');
      this.spinner.append(this.spinnerContent);

      this.innerHTML = '';
      this.append(this.input);
      this.append(this.optionsEl);
      this.append(this.spinner);

      this.addEventListener('keydown', ev => this.keydown(ev));
      this.addEventListener('click', ev => this.click(ev));
      this.addEventListener('mousedown', ev => this.mousedown(ev));
      this.addEventListener('focusin', ev => this.focusin(ev));
      this.input.addEventListener('focusout', ev => this.inputfocusout(ev));
      this.addEventListener('input', ev => this.keyinput(ev));
    }

    setConfig() {
      this.maxOptions = parseInt(this.getAttribute('max-options')) || 5000;
    }

    parseRenderers() {
      const inputRendererExpr = this.getAttribute('input-renderer');
      const optionRendererExpr = this.getAttribute('option-renderer');

      this.inputRenderFn = !!inputRendererExpr
        ? new Function('data', `return ${inputRendererExpr}`)
        : new Function('data', 'return data.text || data.value;');

      this.optionRenderFn = !!optionRendererExpr
        ? new Function('data', `return ${optionRendererExpr}`)
        : new Function('data', 'return data.text || data.value;');
    }

    parseOptions(options) {
      const optionsData = Array.from(options).map(o => this.mapOption(o));
      this._optionsProvider = new UiJsDefaultProvider(optionsData);
    }

    mapOption(option) {
      return option instanceof HTMLOptionElement
        ? { text: option.innerText, value: option.value || option.innerText }
        : option;
    }

    click(ev) {
      if (ev.target instanceof UiJsDropDownOption) {
        ev.target.focus();
        ev.preventDefault();
        ev.stopPropagation();
        this.commit(ev.target);
      }
    }

    mousedown(ev) {
      if (ev.target instanceof HTMLInputElement) {
        this.toggleOptions();
      }
    }

    keydown(ev) {
      let targetItem;
      let selectedItem = this.querySelector('ui-js-dropdown-option:focus');

      if (ev.key == 'ArrowDown') {
          if (this.optionsHidden && this.selected) {
            selectedItem = this.selected;
            this.optionsEl.show();
            selectedItem.focus();
          }
          targetItem = selectedItem ? this.querySelector('ui-js-dropdown-option:focus + ui-js-dropdown-option') : this.querySelector('ui-js-dropdown-option');
      }

      if (ev.key == 'ArrowUp')
          targetItem = selectedItem ? (selectedItem.previousElementSibling || this.input) : undefined;

      if (ev.key == 'End')
          targetItem = this.optionsEl.querySelector('ui-js-dropdown-option:last-child');

      if (ev.key == 'Home')
          targetItem = this.optionsEl.querySelector('ui-js-dropdown-option:first-child');

      if (ev.key == 'Enter') {
        if (ev.target != this.input && selectedItem)
          this.commit(selectedItem);
        else if (ev.target == this.input)
          this.toggleOptions();
      }

      if (targetItem && targetItem.focus) {
          ev.preventDefault();
          this.optionsEl.show();
          targetItem.focus();
      }
    }

    focusin(ev) {
      if (this.suppressFocusEvent) {
        delete this.suppressFocusEvent;
        return;
      }

      if (ev.target == this.input)
        this.loadOptionsFromProvider();
    }

    inputfocusout(ev) {
      if (this.selected === undefined || this.selected === null) {
        if (this.options.length > 1) {
          this.input.value = '';
          this.updateInput();
        } else {
          const selectedItem = this.optionsEl.querySelector('ui-js-dropdown-option:first-child');
          this.commit(selectedItem);
        }
      } else {
        this.updateInput();
      }
    }

    getDefaultOptions() {
      return this._optionsProvider instanceof UiJsDefaultProvider
        ? this._optionsProvider.fetchOptions()
        : [];
    }

    async loadOptionsFromProvider(search)
    {
      this.showSpinner();
      try {
        const optionsData = this._optionsProvider instanceof UiJsDefaultProvider
          ? await this._optionsProvider.fetchOptions(search, this.maxOptions)
          : await this._optionsProvider(search, this.maxOptions);
        this.optionsEl.loadOptions(optionsData, this.optionRenderFn);
        this.optionsEl.show();
      } finally {
        this.hideSpinner();
      }
    }

    showSpinner() {
      this.classList.add('loading');
    }

    hideSpinner() {
      this.classList.remove('loading');
    }

    keyinput(ev) {
      this.dispatchEvent(new CustomEvent(
        'filter', {
          detail: { value: ev.target.value }
        }
      ));

      this.search(ev.target.value);
    }

    disconnectedCallback() {
      // remove event listeners
    }

    attributeChangedCallback(attrName, oldValue, newValue) {
      if (oldValue !== newValue) {
        this[attrName] = this.hasAttribute(attrName);
      }
    }

    toggleOptions() {
      if (this.optionsHidden)
        return this.optionsEl.show();
      this.optionsEl.hide();
    }

    expandOptions() {
      this.optionsEl.show();
    }

    updateInput() {
      if (this.selected)
        this.input.value = this.inputRenderFn(this.selected);
    }

    _search(value) {
      console.log(`Search: ${value}`);
      this.loadOptionsFromProvider(value);
    }

    commit(selected) {
      this.selected = selected;
      this.updateInput();
      this.optionsEl.hide();
      this.suppressFocusEvent = true;
      this.input.focus();
    }

    get options() {
      return this.optionsEl.options;
    }

    get optionsHidden() {
      return this.optionsEl.classList.contains('hide');
    }

    get value() {
      return this.selected ? this.selected.value : undefined;
    }

    set value(value) {
      const option = this.options.find(o => o.value == value);
      if (option) {
        this.selected = option;
        if (this.optionsEl.querySelector(':visible'))
          this.selected.focus();
      }
    }

    set optionsProvider(provider) {
      this._optionsProvider = provider;
    }
});
