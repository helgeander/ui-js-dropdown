import { Slim, Utils } from 'https://unpkg.com/slim-js?module'
import '../lib/ui-js-dropdown-wc.mjs'

const optionRenderExpression = '`${data.value} - ${data.text}`;';

const indexHtml = `
  <h1> ui-js-dropdown-wc samples:</h1>

  <p>
    <label>Fixed width options:</label>
    <ui-js-dropdown class="fixed-width-options" option-renderer="${optionRenderExpression}">
      <option value="1">Item 1</option>
      <option value="2">Item 2</option>
      <option value="3">Item 3</option>
    </ui-js-dropdown>
  </p>

  <p>
    <label>Static dropdown options:</label>
    <ui-js-dropdown option-renderer="${optionRenderExpression}">
      <option value="1">Item 1</option>
      <option value="2">Item 2</option>
      <option value="3">Item 3</option>
      <option value="4">Item 4</option>
      <option value="5">Item 5</option>
      <option value="6">Item 6</option>
      <option value="7">Item 7</option>
      <option value="8">Item 8</option>
      <option value="9">Item 9</option>
      <option value="10">Item 10</option>
      <option value="11">Item 11</option>
      <option value="12">Item 12</option>
    </ui-js-dropdown>
  </p>

  <p>
    <label>Dynamic loaded options:</label>
    <ui-js-dropdown ref="dynamicDropdown" option-renderer="${optionRenderExpression}"></ui-js-dropdown>
  </p>
`;

class IndexView extends Slim {
  constructor() {
    super();
  }

  // Turn off shadow dom
  static get useShadow() {
    return false
  }

  async onCreated() {
    console.log('Created');
  }

  onRender() {
    console.log('Rendered');
    this.querySelector('[ref="dynamicDropdown"]').optionsProvider = this.optionsProvider.bind(this);
  }

  onAdded() {
    console.log('Added');
  }

  onRemoved() {
    console.log('Removed');
  }

  forceUpdate() {
    // Some useful code that affects many properties
    Utils.forceUpdate(this); // run all bindings. Will re-render only changed nodes.
  }

  /**
   * Async options provider
   * @param {string} search "Search value"
   * @param {number} size "Max size of the returned lookup data"
   */
  async optionsProvider(search, size) {
    await this.delay(4000);
    const response = await fetch(`data/options-data.json?search=${search}&size=${size}`);
    const optionsData = await response.json();
    return optionsData.filter(data => data.text.startsWith(search) || data.value.toString().startsWith(search) || !search);
  }

  async delay(millisec) {
    return new Promise(resolve => {
      setTimeout(() => { resolve('') }, millisec);
    });
  }
}

Slim.element('index-view', indexHtml, IndexView);