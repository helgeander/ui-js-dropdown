const uidropdown = Ractive.extend({
	isolated: true,
	template: '<div class="ui-js-dropdown"></div>',
	oncomplete: function() {
		var el = this.find('div');
		var dropdown = new UiJsDropdown(
						this.get('options'),
						el,
						this.get('config'));

    dropdown.render();
	}
});

const ractive = new Ractive({
  target: '#main',
  template: '#template',
  components: { uidropdown },
  data: {
    formData: {
      value1: null,
      value2: null
    },
    dropdown1: {
      options: [
        { text: "Orange", value: 1 },
        { text: "Apple", value: 2 },
        { text: "Lemon", value: 3 },
        { text: "Pear", value: 4 },
        { text: "Plum", value: 5 }
      ],
      config: {

      }
    },
    dropdown2: {
      options: [],
      config: {

      }
    }
  }
});

